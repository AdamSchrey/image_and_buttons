#!/bin/bash

set -Eeou pipefail

echo "ARCH: ${ARCH}"

apt-get update
apt-get -y upgrade

apt-get -y install libgtk-3-dev:${ARCH}

cd "${ROOT}"
case "${ARCH}" in
	"armhf") arm-linux-gnueabihf-gcc main.c -o image_and_buttons `pkg-config --cflags --libs gtk+-3.0` ;;
	"arm64") aarch64-linux-gnu-gcc main.c -o image_and_buttons `pkg-config --cflags --libs gtk+-3.0` ;;
	*)	echo "${ARCH} is not armhf or arm64, using normal gcc command"
    	gcc main.c -o image_and_buttons `pkg-config --cflags --libs gtk+-3.0`
esac

exit
