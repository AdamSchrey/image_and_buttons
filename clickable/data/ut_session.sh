#!/bin/bash

if [ ! -d $HOME/.config/image_and_buttons ]; then
	mkdir -p $HOME/.config/image_and_buttons
	cp -R {entered_text,messages,image,header,entries,buttons,0.png,start.sh} $HOME/.config/image_and_buttons
fi

export GTK_IM_MODULE=maliitphablet
export GDK_SCALE=2
image_and_buttons


exit

