DESCRIPTION
--------------------------------------------------------------------

image_and_buttons is a simple and highly configurable image editor.
The idea is to have a GUI that displays an image and a configurable
number of buttons and entries.
For every button and entry exists a shell script that does the image
manipulation.
The clicked or touched position on the image will be recognized and
can be used by a shell script.


INSTALLATION
--------------------------------------------------------------------

--

Following packages should be installed, if you want to install the
program on your local PC (package names in Debian 10):
gcc libc-dev libgtk-3-dev

Execute the script install_deb.sh as root or with sudo
(example: sudo ./install_deb.sh) to install the program on a Debian
based system (like PureOS or Ubuntu).

Execute compile_here.sh, if you want to run the program without a
system specific installation.
The executable will have the name: image_and_buttons

--

Clickable should be installed, if you want to install the
program on an Ubuntu Touch device or another device that supports
clickable files, this device should be connected to your PC:
https://clickable-ut.dev/en/latest/

Execute the script install_clickable.sh as root or with sudo
(example: sudo ./install_clickable.sh) to install the program on a
system that supports clickable files (like Ubuntu Touch).
You maybe also need to install maliit-inputcontext-gtk3
and maliit-inputcontext-gtk2 on your device to have an on screen
keyboard that supports GTK applications.

--

CONFIG
--------------------------------------------------------------------

image_and_buttons searches for the directory
$HOME/.config/image_and_buttons that will be created for every
user in /home during the installation.
The current directory will be used, should this directory not exist.

Any config of image_and_buttons should include following files and
directories:

start.sh
1.png and 2.png (can be created by start.sh)
entries/
	text.txt (32 or more lines)
	A widget will be added for every other file in the directory.
	Every other file in the directory should be executable.
image/
	image.sh
messages/
	temp_message.txt should be here, if start.sh terminates with
	an error.
buttons/
	text.txt (32 or more lines)
	A widget will be added for every other file in the directory.
	Every other file in the directory should be executable.
entered_text/
	1.txt
header/
	text.txt (32 or more lines)
	A widget will be added for every other file in the directory.
	Every other file in the directory should be executable.


DEFAULT CONFIG
--------------------------------------------------------------------

The default config depends on (package names in Debian 10):
imagemagick zenity xdg-utils libnotify4 bc
image_and_buttons can be installed without this packages and it is
possible to create a config that does not depend on this packages.

The image 0.png of the default config is a slightly modified version
of the ImageMagick logo by the ImageMagick LLC and the pepper
originates from GIMP and was created by Adrian Likins.


LICENSE
--------------------------------------------------------------------
-------------------------------------------
GNU General Public License:
-------------------------------------------

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be entertain,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------
ImageMagick (0.png of the default config):
-------------------------------------------

Copyright 2020 ImageMagick LCC

Licensed under the ImageMagick License (the "License"); you may not use
this file except in compliance with the License.  You may obtain a copy
of the License at

https://imagemagick.org/script/license.php

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
License for the specific language governing permissions and limitations
under the License.