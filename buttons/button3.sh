#!/bin/bash

#get the coordintes
pos2="$(sed '2q;d' entered_text/1.txt)"
pos1="$(sed '1q;d' entered_text/1.txt)"

if [[ "$pos1" == "" ]]; then
	pos1="0 0"
fi
if [[ "$pos2" == "" ]]; then
	pos2="0 0"
fi

x1="${pos1%% *}"
y1="${pos1##* }"
x2="${pos2%% *}"
y2="${pos2##* }"

echo $x1 $y1 $x2 $y2

if [[ "$x2" -gt "$x1" ]]; then
	from_x=$x1
	new_x=$(($x2 - $x1))
else
	from_x=$x2
	new_x=$(($x1 - $x2))
fi

if [[ "$y2" -gt "$y1" ]]; then
	from_y=$y1
	new_y=$(($y2 - $y1))
else
	from_y=$y2
	new_y=$(($y1 - $y2))
fi

echo $new_x $new_y $from_x $from_y

convert 1.png -crop "$new_x"x"$new_y"+$from_x+$from_y +repage 1.png
#creating transparent 2.png with a max. width and height of 300
ow="$(identify -format '%w' 1.png)"
oh="$(identify -format '%h' 1.png)"
if [ $ow -gt $oh ]; then
	w=300
	h=$(bc <<<"scale=0; 300 * $oh / $ow")
else
	h=300
	w=$(bc <<<"scale=0; 300 * $ow / $oh")
fi
convert -size "$w"x"$h" canvas:transparent 2.png

sed -i "1s/.*//" entered_text/1.txt
sed -i "2s/.*//" entered_text/1.txt

exit
