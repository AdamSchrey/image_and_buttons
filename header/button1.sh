#!/bin/bash

FILE=($(zenity --file-selection --save --confirm-overwrite --title  "Export image"))
echo $FILE
if [[ $FILE == "" ]]; then
	exit
fi
convert 1.png $FILE
if [[ $? -eq 0 ]]; then
	notify-send "saved image at $FILE"
else
	notify-send "could not save image at $FILE"
fi

exit