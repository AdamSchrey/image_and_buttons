#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <gtk/gtk.h>
#include <dirent.h>
#include <string.h>
#include <stdbool.h>

#define MAXWIDGETS 32 //there can be max 32 widgets of one type (header buttons, buttons, entries)
#define MAXWIDGETTEXT 32
#define MAXSCRIPTTEXT 32

//struct to pass more data through G_CALLBACK:
struct more_data{
  	GtkWidget* im; //image to call gtk_widget_queue_draw
  	GtkWidget** ent; //array of entries to call update_entries
	int number_ent; //number of entries to call update_entries
    char sh[MAXSCRIPTTEXT]; //string with the script that will be executed
}; 


//modified functions to prevent mistakes when dealing with strings:
//sizeof is not able to get the size of string passed as parameter => third parameter size is needed in strncpy_mod and strncat_mod

static int strcmp_mod(const void* a, const void* b) 
{
	//compare first the length of the strings and compare the characters later
	if (strlen(*(const char**)a) < strlen(*(const char**)b)){return 0;}
	if (strlen(*(const char**)b) < strlen(*(const char**)a)){return 1;}
    return strcmp(*(const char**)a, *(const char**)b); 
}

void strncpy_mod (char *restrict dest, const char *restrict source, const int size)
{
	strncpy (dest, source, size - 1);
	dest[size - 1] = '\0';
}

void strncat_mod (char *restrict dest, const char *restrict source, const int size)
{
	strncat(dest, source, size - strlen(dest) - 1);
}

void get_text_of_widgets(char *path_to_text, char** text_array, int number){
	FILE *fp = NULL;
	char line[MAXWIDGETTEXT];

	fp = fopen(path_to_text, "r");
	if (!fp) {
		perror ("File open error!\n");
	}

	for (int ntext = 0; ntext < number; ntext++){
		if (!(feof(fp)) && fgets(line,sizeof(line),fp)) {
			//'\0' is in each line after '\n', to get rid of \n and make sure it is not more than MAXWIDGETTEXT: 
			if (strlen(line) >= MAXWIDGETTEXT){
				line[MAXWIDGETTEXT]  = '\0';
			}else{
				line[strlen(line)-1]  = '\0';
			}
			text_array[ntext] = malloc(MAXWIDGETTEXT*sizeof(char));			
			strncpy_mod(text_array[ntext],line,MAXWIDGETTEXT*sizeof(char));
		}
	}
	fclose (fp);
}

void update_entries(GtkWidget *(*entry_array), int number){
	FILE *fp = NULL;
	char line[1000];

	fp = fopen("entered_text/1.txt", "r");
	if (!fp) {
		perror ("File open error!\n");
	}

	for (int n = 0; n < number; n++){
		if (!(feof(fp)) && fgets(line,sizeof(line),fp)) {
			line[strlen(line)-1]  = '\0';
			gtk_entry_set_text(GTK_ENTRY(entry_array[n]),line);
		}
	}
	fclose (fp);
}

void get_error_text(char *path_to_text, char *error_text){
	FILE *fp = NULL;
	char *line = NULL;
	size_t line_buf_size = 0;
	int line_count = 0;
	ssize_t line_size;

	fp = fopen(path_to_text, "r");
	if (!fp) {
		perror ("File open error!\n");
	}

	line_size = getline(&line, &line_buf_size, fp);

	while (line_size >= 0){
		line_count++;
		//g_print("line[%06d]: chars=%06zd, buf size=%06zu, contents: %s", line_count,
		//line_size, line_buf_size, line);
		if (strlen(line) >= 1000){
			line[999]  = '\n';
			line[1000] = '\0';
		}		
		strncat_mod(error_text,line,1000*sizeof(char));
		line_size = getline(&line, &line_buf_size, fp);
	}

	free(line);
	line = NULL;
	fclose (fp);
}

int get_elem_of_dir(char *path, char *(*scripts)){

	g_print("\n\nGetting scripts of: %s\n", path);
    
	DIR *d;
    struct dirent *dir;
    d = opendir(path);

	if (!d) {
		perror ("Directory open error!\n");
	}

	int number = 0;

    if (d){
        while (( (dir = readdir(d)) != NULL ) && ( number <= (MAXWIDGETS-1) ))
		{
			if ((dir->d_type == DT_REG) && (strcmp(dir->d_name,"text.txt"))){
            	g_print("%s\n", dir->d_name);
				char temp_f[MAXSCRIPTTEXT] = "./";

				strncat_mod(temp_f, path, sizeof(temp_f));
				strncat_mod(temp_f, "/", sizeof(temp_f));
				strncat_mod(temp_f, dir->d_name, sizeof(temp_f));

				scripts[number] = malloc(MAXSCRIPTTEXT*sizeof(char));
				strncpy_mod(scripts[number], temp_f, MAXSCRIPTTEXT*sizeof(char));

				number += 1;
			}
        }
        closedir(d);
    }

	g_print("\nSorting script names:\n");	

    qsort(scripts, number, sizeof(char*), strcmp_mod);

	for (int ntext = 0; ntext < number; ntext++){
		g_print(scripts[ntext]);
		g_print(" ");
	}

    return(number);

}

static gboolean pos_on_image (GtkWidget *event_box, GdkEventButton *event, struct more_data *d)
{
	float width=gtk_widget_get_allocated_width(d->im);
	float height=gtk_widget_get_allocated_height(d->im);
	
	GdkPixbuf *pixbuf;
	pixbuf=gdk_pixbuf_new_from_file("1.png",NULL);
	float o_width = gdk_pixbuf_get_width (pixbuf);
	float o_height = gdk_pixbuf_get_height (pixbuf);

	if ((width/height) <= (o_width/o_height)){
		height = width*(o_height/o_width);
	}else{
		width = height*(o_width/o_height);
	}

	if (event->x <= width && event->y <= height){
		g_print ("Event box clicked at coordinates %f,%f\n", event->x, event->y);
		g_print ("W: %f x H:%f V:%f\n", width, height, width/height); //ratio
		
		float rel_pos_x = event->x/width;
		float rel_pos_y = event->y/height;
		int pos_x = (float)((event->x/width)*o_width)+1;
		int pos_y = (float)((event->y/height)*o_height)+1;
		g_print ("Relative position: x:%d y:%d\n", pos_x, pos_y);
		char command[MAXSCRIPTTEXT];
		sprintf(command, "image/image.sh %d %d", pos_x, pos_y);
		system(command);
		gtk_widget_queue_draw(d->im);
		update_entries(d->ent, d->number_ent);
	}else{
		g_print("Not on image\n");
	}

  return TRUE;
}

void f_button(GtkWidget *button, struct more_data *d)
{
    system(d->sh);
    gtk_widget_queue_draw(d->im);
	update_entries(d->ent, d->number_ent);
}

void f_entry_activate (GtkEntry *entry, struct more_data *d)
{
    char command[1000];
	strncpy_mod(command,d->sh,sizeof(command));
	strncat_mod(command," activate",sizeof(command));
	strncat_mod(command," ",sizeof(command));
	strncat_mod(command,gtk_entry_get_text(entry),sizeof(command));
    g_print("\n%s\n",command);
    system(command);
    gtk_widget_queue_draw(d->im);
	update_entries(d->ent, d->number_ent);
}

void f_entry_focus (GtkEntry *entry, GdkEvent *event, struct more_data *d)
{
    char command[1000];
	strncpy_mod(command,d->sh,sizeof(command));
	strncat_mod(command," focus",sizeof(command));
	strncat_mod(command," ",sizeof(command));
	strncat_mod(command,gtk_entry_get_text(entry),sizeof(command));
    g_print("\n%s\n",command);
    system(command);
    gtk_widget_queue_draw(d->im);
	update_entries(d->ent, d->number_ent);
}

gboolean draw_picture(GtkWidget *im, cairo_t *cr, GdkPixbuf *pixbuf)
{
	GdkPixbuf *temp1;
	GdkPixbuf *temp2;

	float width=gtk_widget_get_allocated_width(im);
	float height=gtk_widget_get_allocated_height(im);

	pixbuf=gdk_pixbuf_new_from_file("1.png",NULL);
	float o_width = gdk_pixbuf_get_width (pixbuf);
	float o_height = gdk_pixbuf_get_height (pixbuf);

	if ((width/height) <= (o_width/o_height)){
		height = width*(o_height/o_width);
	}else{
		width = height*(o_width/o_height);
	}

	temp1=gdk_pixbuf_new_from_file ("1.png", NULL);
	temp2=gdk_pixbuf_new_from_file ("2.png", NULL);

	if ((o_width < 400) && (o_height < 400)){ //every image smaller than 400x400 will most likely upscale
		//GDK_INTERP_NEAREST is good for scaling up and making the pixels visible
		temp1=gdk_pixbuf_scale_simple (temp1, width, height, GDK_INTERP_NEAREST);
		temp2=gdk_pixbuf_scale_simple (temp2, width, height, GDK_INTERP_NEAREST);
	}else{
		//GDK_INTERP_BILINEAR is good for scaling down
		temp1=gdk_pixbuf_scale_simple (temp1, width, height, GDK_INTERP_BILINEAR);
		temp2=gdk_pixbuf_scale_simple (temp2, width, height, GDK_INTERP_BILINEAR);
	}


	gdk_cairo_set_source_pixbuf(cr, temp1, 0, 0);
	cairo_paint(cr);
	gdk_cairo_set_source_pixbuf(cr, temp2, 0, 0);
	cairo_paint(cr);

	g_object_unref(temp1);
	g_object_unref(temp2);
	return FALSE;
}

int main(int argc, char **argv) {

	char config_path[1000];
	strncpy_mod(config_path, getenv("HOME"), sizeof(config_path));
	strncat_mod(config_path, "/.config/image_and_buttons", sizeof(config_path));
	g_print("\nTry to change working directory of image_and_buttons to: %s", config_path);
	int chdir_return = chdir(config_path);
	if (chdir_return != 0){
		g_print("\nFailed to change working directory of image_and_buttons to: %s", config_path);
		g_print("\nUse instead: ");
	}else{
		g_print("\nSuccess! Working directory: ");
	}
	
	system("pwd");

	GdkPixbuf *pixbuf;

    GtkWidget *window, *header_bar, *grid, *event_box, *image, *start_label;
	//creating the Widget arrays later in the code from the number returned by
	//get_elem_of_dir will cause problems => create Widget arrays with the number MAXWIDGETS
	GtkWidget* entries[MAXWIDGETS];
	GtkWidget* buttons[MAXWIDGETS];
	GtkWidget* header_buttons[MAXWIDGETS];

    gtk_init(&argc, &argv);

    window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title(GTK_WINDOW(window),"Image and Buttons");
    gtk_window_set_default_size(GTK_WINDOW(window), 500, 400);

	gtk_window_set_default_icon_name("image_and_buttons");

    g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL);

    grid = gtk_grid_new();
    gtk_container_add(GTK_CONTAINER(window), grid);

	char* script_array[3*MAXWIDGETS];
	struct more_data data_array[3*MAXWIDGETS];

	bool start_error;
	bool non_existing_files;

	//check if start.sh and messages/ exist and execute start.sh if they exist
	if( (access( "start.sh", F_OK ) != -1) && (access( "messages", F_OK ) != -1) ) {
		start_error = system("./start.sh");
	} else {
		start_error = 1;
		non_existing_files = 1;
	}

	g_print("start_error: %d",start_error);

	//print an error text if files do not exist or start.sh returns an error
	if (start_error){

		char error_text[10000]="";

		if (non_existing_files){
			strncpy_mod(error_text,
						"Error: Could not find [start.sh] or [messages/].\nThey should be in: ",
						sizeof("Error: Could not find [start.sh] or [messages/].\nThey should be in: "));
			strncat_mod(error_text, config_path, sizeof(config_path));
		}else{
			get_error_text("messages/temp_message.txt", error_text);
		}

		start_label = gtk_label_new(error_text);
		gtk_label_set_selectable (GTK_LABEL(start_label),TRUE);
		gtk_label_set_line_wrap (GTK_LABEL(start_label),TRUE);
		gtk_grid_attach(GTK_GRID(grid),start_label, 0, 0, 3, 1);
	
	}else{

		image = gtk_image_new();
		gtk_widget_set_hexpand(image, TRUE);
		gtk_widget_set_vexpand(image, TRUE);
		g_signal_connect(image, "draw", G_CALLBACK(draw_picture), pixbuf);

		char* entry_scripts[MAXWIDGETS];
		char* button_scripts[MAXWIDGETS];
		char* header_scripts[MAXWIDGETS];
		int n_entries = get_elem_of_dir("entries", entry_scripts);
		int n_buttons = get_elem_of_dir("buttons", button_scripts);
		int n_header_buttons = get_elem_of_dir("header", header_scripts);

		char* entry_text[n_entries];
		get_text_of_widgets("entries/text.txt", entry_text, n_entries);
		char* button_text[n_buttons];
		get_text_of_widgets("buttons/text.txt", button_text, n_buttons);

		for (int i = 0; i < n_entries+n_buttons+n_header_buttons; ++i){
			data_array[i].im = image;
			data_array[i].ent = entries;
			data_array[i].number_ent = n_entries;
		}

		if (n_header_buttons){

			header_bar = gtk_header_bar_new();
			gtk_header_bar_set_show_close_button(GTK_HEADER_BAR(header_bar),TRUE);
			gtk_window_set_titlebar(GTK_WINDOW(window),header_bar);

			char* header_button_text[n_header_buttons];
			get_text_of_widgets("header/text.txt", header_button_text, n_header_buttons);

			for (int i = 0; i < n_header_buttons; ++i){
				strncpy_mod(data_array[i].sh,header_scripts[i],sizeof(data_array[i].sh));
				header_buttons[i] = gtk_button_new_with_label(header_button_text[i]);
				g_signal_connect(header_buttons[i], "clicked", G_CALLBACK(f_button), &data_array[i]);
				gtk_header_bar_pack_start(GTK_HEADER_BAR(header_bar), header_buttons[i]);
			}
		}
		

		int l;
		int r;
		

		for (int i = 0; i < n_buttons; ++i){
			strncpy_mod(data_array[i+n_header_buttons].sh,button_scripts[i],sizeof(data_array[i+n_header_buttons].sh));
			buttons[i] = gtk_button_new_with_label(button_text[i]);
			g_signal_connect(buttons[i], "clicked", G_CALLBACK(f_button), &data_array[i+n_header_buttons]);
			
			l = i;			
			if(i == 0){
				l = 1;
			}
			l /= 3;
			r = (i%3);

			gtk_grid_attach(GTK_GRID(grid), buttons[i], r, 1+l, 1, 1);
		}
		
		for (int i = n_buttons; i < n_entries+n_buttons; ++i){
			entries[i-n_buttons] = gtk_entry_new();
			gtk_entry_set_placeholder_text(GTK_ENTRY(entries[i-n_buttons]), entry_text[i-n_buttons]);
			strncpy_mod(data_array[i+n_header_buttons].sh,entry_scripts[i-n_buttons],sizeof(data_array[i+n_header_buttons].sh));
			g_signal_connect(entries[i-n_buttons], "focus_out_event", G_CALLBACK(f_entry_focus), &data_array[i+n_header_buttons]);
			g_signal_connect(entries[i-n_buttons], "activate", G_CALLBACK(f_entry_activate), &data_array[i+n_header_buttons]);
			gtk_grid_attach(GTK_GRID(grid), entries[i-n_buttons], 0, i, 3, 1);
		}

		

		event_box = gtk_event_box_new ();
		gtk_container_add (GTK_CONTAINER (event_box), image);
  		g_signal_connect (G_OBJECT (event_box),"button_press_event",
							G_CALLBACK (pos_on_image),&data_array[0]);
		gtk_grid_attach(GTK_GRID(grid),event_box, 0, 0, 3, 1);	
	}

    gtk_widget_show_all(window);
    gtk_main();

    return 0;
}

//gcc main.c -o a.out `pkg-config --cflags gtk+-3.0` `pkg-config --libs gtk+-3.0`
