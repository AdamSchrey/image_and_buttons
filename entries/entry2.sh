#!/bin/bash
ox="$(($2))"
oy="$(($3))"
case "$ox" in
	"") exit ;;
esac
case "$oy" in
	"") exit ;;
esac
ow="$(identify -format '%w' 1.png)"
oh="$(identify -format '%h' 1.png)"
w="$(identify -format '%w' 2.png)"
h="$(identify -format '%h' 2.png)"

x=$(bc <<<"scale=0; $ox * $w / $ow")
y=$(bc <<<"scale=0; $oy * $h / $oh")

sed -i '2s/.*/'"$ox $oy"'/' entered_text/1.txt

case "$1" in
	activate) convert 2.png -stroke blue -strokewidth 1 -draw "line   $x,0 $x,$h" -stroke lightblue -strokewidth 1 -draw "line   0,$y $w,$y" 2.png;; #executes with enter
	focus) convert 2.png -stroke blue -strokewidth 1 -draw "line   $x,0 $x,$h" -stroke lightblue -strokewidth 1 -draw "line   0,$y $w,$y" 2.png ;; #executes by focus change
esac