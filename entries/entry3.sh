#!/bin/bash

arguments=("${@}")
command="${arguments[@]:1}"

sed -i '3s/.*/'"$command"'/' entered_text/1.txt

case "$1" in
	activate) ./buttons/button4.sh ;; #executes with enter
	focus) : ;; #executes by focus change
esac